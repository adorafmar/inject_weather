const fs = require('fs');
const csv = require('csv-parser');
const ftp = require("basic-ftp")
const AWS = require('aws-sdk');
const axios = require('axios');

const BUCKET_NAME = 'utpower';

const one_minute = 1000*60;
const one_hour = one_minute*60;
const one_day = 24*one_hour;

const ID = 'AKIATSA45FQDSKJYELX5';
const SECRET = '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';

const s3 = new AWS.S3({
    accessKeyId: ID,
    secretAccessKey: SECRET
});
const WSIDAILYOBS = "WSIDailyObs.csv";
const filename_local = ""+Math.floor((new Date()).getTime()/1000/60/60/24)*1000*60*60*24-1000*60*60*24+".csv";

const uploadFile = (filename) => {
    const fileContent = fs.readFileSync(filename);
    const params = {
        Bucket: BUCKET_NAME,
        Key: "weather_ftp_dynamic/"+(new Date()).getTime()+".csv",
        Body: fileContent
    };
    s3.upload(params, function(err, data) {
        if (err) {
            throw err
        }
        console.log(`File uploaded successfully. ${data.Location}`)
        try {
            fs.unlinkSync(filename)
        } catch(err) {
            console.error(err)
        }

    });
};

function write_csv(weather_data){
    const createCsvWriter = require('csv-writer').createObjectCsvWriter;
    const csvWriter = createCsvWriter({
      path: filename_local,
      header: [
        {id: 'station', title: 'station'},
        {id: 'time', title: 'time'},
        {id: 'hdd', title: 'hdd'},
        {id: 'cdd', title: 'cdd'},
        {id: 'avg_temperature', title: 'avg_temperature'},
      ]
    });
    csvWriter
      .writeRecords(weather_data)
      .then(()=> {
        console.log('The CSV file was written successfully');
        uploadFile(filename_local);
    });    
}

function read_csv(){
    var weather_data = []
    fs.createReadStream(WSIDAILYOBS)
      .pipe(csv())
      .on('data', (row) => {
        console.log(row);
        var new_row = {};
        new_row.station = row.STATION;
        new_row.time = (new Date(row.DATE).getTime());
        new_row.hdd = row.HDD;
        new_row.cdd = row.CDD;
        new_row.avg_temperature = row.AVGTEMP;
        weather_data.push(new_row);
      })
      .on('end', () => {
        console.log('CSV file successfully read');
        write_csv(weather_data);
      });
}

async function downloadFile(filename) {
    const client = new ftp.Client()
    client.ftp.verbose = true
    try {
        await client.access({
            host: "wsiftp.wsi.com",
            user: "utaustin",
            password: "Longh0rns",
            secure: false 
        })
        console.log(await client.list())
        await client.downloadTo(filename,filename);
        read_csv();
    }
    catch(err) {
        console.log(err)
    }
    client.close()
}

downloadFile(WSIDAILYOBS)
